package unalcol.agents.examples.labyrinth.multeseo.isi116.slayers;
import unalcol.agents.simulate.util.SimpleLanguage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MultiTeseoSlayers extends MultiTeseoAgentProgram {

	public MultiTeseoSlayers(SimpleLanguage language) {
		super(language);
		// reiniciar();
	}

	/*
	 * Este metodo retorna alguna de las siguientes opciones -1: Ninguna Accion
	 * 0: Avanzar 1: 1 Giro a la derecha y avanzar 2: 2 Giros a la derecha y
	 * avanzar 3: 3 Giros a la derecha y avanzar
	 */
	@Override
	public int accion(boolean PF, boolean PD, boolean PA, boolean PI, boolean MT, boolean AF, boolean AD, boolean AA,
			boolean AI) {

		// Encontro la meta
		if (MT)
			return -1;

		// El nodo donde esta el agente no esta pendiente de visitar
		if (nodosPendientes.contains(fila + "/" + columna)) {
			nodosPendientes.remove(fila + "/" + columna);
		}

		// Crear un nuevo nodo si es el caso
		if (!grafo.contieneNodo(fila + "/" + columna)) {
			grafo.addNodo(fila + "/" + columna);
		}

		int filaAnterior = fila;
		int columnaAnterior = columna;
		ArrayList<Integer> opciones = new ArrayList<>();

		// El agente toma alguna de las siguientes posiciones
		if (!PF && decision(0)) {
			opciones.add(0);
		}
		if (!PD && decision(1)) {
			opciones.add(1);
		}
		if (!PA && decision(2)) {
			opciones.add(2);
		}
		if (!PI && decision(3)) {
			opciones.add(3);
		}

		// Si no hay a donde ir se busca un nodo con mas caminos
		if (opciones.isEmpty()) {
			// En caso de no existir nodos pendientes se reinicia el agente
			if (nodosPendientes.isEmpty()) {
				//reiniciar();
				return -1;
			}

			// Buscar el nodo pendiente mas cercano
			List<String> listaRutaCortaTemporal=null;
			List<String> listaRutaCorta = null;
			int longitudMinima = (int) Double.POSITIVE_INFINITY;
			int indice = -1;

			for (int a = 0; a < nodosPendientes.size(); a++) {
				try {
					// Buscar la ruta mas corta
					
//					listaRutaCortaTemporal = Dijkstra.encontrarRuta(grafo, fila + "/" + columna,nodosPendientes.get(a));
					
//					listaRutaCortaTemporal = rutaCorta.getPath(fila + "/" + columna, nodosPendientes.get(a));
				} catch (Exception e) {
					//reiniciar();
					return -1;
				}
				if (listaRutaCortaTemporal.size() < longitudMinima) {
					longitudMinima = listaRutaCortaTemporal.size();
					indice = a;
					listaRutaCorta = new ArrayList<>(listaRutaCortaTemporal);
				}
				listaRutaCortaTemporal.clear();
			}

			String nodoDestino = nodosPendientes.get(indice);

			crearCamino(listaRutaCorta);

			// Evitar que el agente se quede quieto luego de encontrarse con
			// otro 5 veces
			if (contador == 10) {
				//reiniciar();
			}
			contador++;
			return -1;
		}

		if (opciones.size() > 1) {
			nodosPendientes.add(fila + "/" + columna);
		} else {
			nodosPendientes.remove(fila + "/" + columna);
		}

		// entre las opciones disponibles, elige una de manera aleatoria
		int numeroAccion = opciones.get((int) (Math.random() * (opciones.size())));

		// En caso de encontrar otro agente esperar un momento, luego de 5 veces
		// reiniciar
		if ((numeroAccion == 0 && AF) || (numeroAccion == 1 && AD) || (numeroAccion == 2 && AA)
				|| (numeroAccion == 3 && AI)) {
			if (contador == 10) {
				//reiniciar();
			}
			contador++;
			return -1;
		}
		// Cambiar de orientacion si el otro agente se mueve
		if (0 <= numeroAccion && numeroAccion <= 3) {
			int salida[] = cambiarOrientacion(orientacion, numeroAccion);
			orientacion = salida[0];
			fila = salida[1];
			columna = salida[2];
		}

		// Crear un nuevo enlace
		crearEnlace(fila + "/" + columna, filaAnterior + "/" + columnaAnterior);

		return numeroAccion;
	}

	private Grafo grafo = new Grafo();
//	private DijkstraShortestPath<String, String> rutaCorta = new DijkstraShortestPath<String, String>(grafo);
	private ArrayList<String> nodosPendientes = new ArrayList<>();
	private int fila = 0;
	private int columna = 0;
	private int orientacion = 0;
	private int contador = 0;

	private void reiniciar() {
		fila = columna = orientacion = contador = 0;
		grafo = new Grafo();
		//rutaCorta = new DijkstraShortestPath<String, String>(grafo);
		nodosPendientes = new ArrayList<>();
//		System.out.println("MultiTeseoSlayers reiniciado");
	}

	private int[] cambiarOrientacion(int orientacion2, int giro) {
		int[] salida = { 0, fila, columna };
		salida[0] = (orientacion2 + giro) % 4;
		if (salida[0] == 0) { // Avanza al norte
			salida[1] = fila - 1;
		}
		if (salida[0] == 1) { // Avanza al este
			salida[2] = columna + 1;
		}
		if (salida[0] == 2) { // Avanza al sur
			salida[1] = fila + 1;
		}
		if (salida[0] == 3) { // Avanza al oeste
			salida[2] = columna - 1;
		}
		return salida;
	}

	private void crearEnlace(String nodoPrincipal, String nodoTemporal) {
		if (!grafo.contieneNodo(nodoPrincipal)) {
			grafo.addNodo(nodoPrincipal);
		}
		if (!grafo.contieneNodo(nodoTemporal)) {
			grafo.addNodo(nodoTemporal);
			contador = 0;
		}

		String nombre = "(" + nodoTemporal + "&" + nodoPrincipal + ")";
		String nombre2 = "(" + nodoPrincipal + "&" + nodoTemporal + ")";

		if (!grafo.contieneEdge(nombre) && !grafo.contieneEdge(nombre2)) {
			grafo.addEdge(nombre, nodoTemporal, nodoPrincipal);
		}
//		System.out.println(grafo.getVertices().size());
	}

	private boolean decision(int giro) {
		int filColTemporal[] = cambiarOrientacion(orientacion, giro);
		if (!grafo.contieneNodo(filColTemporal[1] + "/" + filColTemporal[2])) {
			return true;
		} else {
			crearEnlace(fila + "/" + columna, filColTemporal[1] + "/" + filColTemporal[2]);
			return false;
		}
	}

	private void crearCamino(List<String> listaRutaCorta) {
		String enlace;
		String[] arrayDeNodos;
		String nodo1;
		String nodo2;
		for (String listaRutaCorta1 : listaRutaCorta) {
			enlace = listaRutaCorta1;
			arrayDeNodos = enlace.split("&");
			nodo1 = arrayDeNodos[0].substring(1, arrayDeNodos[0].length());
			nodo2 = arrayDeNodos[1].substring(0, arrayDeNodos[1].length() - 1);
			grafo.removerEdge(enlace);
			grafo.removerNodo(nodo1);
			grafo.removerNodo(nodo2);
		}
	}

	private int generaAleatorio(int aStart, int aEnd) {
		Random random = new Random();
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		// get the range, casting to long to avoid overflow problems
		long range = (long) aEnd - (long) aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long) (range * random.nextDouble());
		int randomNumber = (int) (fraction + aStart);

		return randomNumber;
	}

}
