package unalcol.agents.examples.labyrinth.multeseo.isi116.slayers;

public class Nodo {
	
	private String datos;
	
	public Nodo(String datos){
		this.setDatos(datos);
	}

	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}
	
	

}
