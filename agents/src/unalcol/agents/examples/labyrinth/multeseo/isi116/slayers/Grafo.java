package unalcol.agents.examples.labyrinth.multeseo.isi116.slayers;

import java.util.ArrayList;

public class Grafo{

	private ArrayList<Nodo> nodos;
	private ArrayList<Edge> edges;

	public Grafo() {
		nodos = new ArrayList<Nodo>();
		edges = new ArrayList<Edge>();
	}
	
	public void addNodo(String datos){
		nodos.add(new Nodo(datos));
	}
	
	public void addEdge(String nombre, String nodo1, String nodo2){
		edges.add(new Edge(nombre,nodo1,nodo2));
	}
	
	public void removerNodo(String dato){
		for (int i = 0; i < nodos.size(); i++) {
			if(nodos.get(i).getDatos().equals(dato)){
				nodos.remove(i);
				break;
			}
		}
	}
	
	public void removerEdge(String nombre){
		for (int i = 0; i < edges.size(); i++) {
			if(edges.get(i).getNombre().equals(nombre)){
				edges.remove(i);
				break;
			}
		}
	}
	
	public boolean contieneNodo(String dato){
		boolean resultado = false;
		
		for (int i = 0; i < nodos.size(); i++) {
			if(nodos.get(i).getDatos().equals(dato)){
				resultado=true;
				break;
			}
		}
		
		return resultado;
	}
	
	public boolean contieneEdge(String nombre){
		boolean resultado = false;
		
		for (int i = 0; i < edges.size(); i++) {
			if(edges.get(i).getNombre().equals(nombre)){
				resultado=true;
				break;
			}
		}
		
		return resultado;
	}
	
	public ArrayList<Nodo> getNodos(){
		return nodos;
	}
	
	public ArrayList<Edge> getEdges(){
		return edges;
	}

}
