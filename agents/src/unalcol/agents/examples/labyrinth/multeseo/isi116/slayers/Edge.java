package unalcol.agents.examples.labyrinth.multeseo.isi116.slayers;

public class Edge {
	
	private String nombre;
	private String nodo1;
	private String nodo2;
	
	public Edge(String nombre,String n1, String n2){
		this.nombre=nombre;
		nodo1=n1;
		nodo2=n2;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNodo1() {
		return nodo1;
	}
	public void setNodo1(String nodo1) {
		this.nodo1 = nodo1;
	}
	public String getNodo2() {
		return nodo2;
	}
	public void setNodo2(String nodo2) {
		this.nodo2 = nodo2;
	}

}
